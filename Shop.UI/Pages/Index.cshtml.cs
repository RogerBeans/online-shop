﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Shop.Application.CreateProducts;
using Shop.Application.GetProducts;
using Shop.Application.Products;
using Shop.Database;
//using Shop.Database;

namespace Shop.UI.Pages
{
    public class IndexModel : PageModel
    {
        private ApplicationDbContext _context;

        public IndexModel(ApplicationDbContext ctx) {
            _context = ctx;
        }

        //wherever we bind is our main model
        [BindProperty]
        public Application.CreateProducts.ProductViewModel Product { get; set; }

       
        public IEnumerable<Application.GetProducts.ProductViewModel> Products { get; set; }

        public void OnGet()
        {
            Products = new GetProducts(_context).DO();

        }

        public async Task<IActionResult> OnPost()
        {
            await new CreateProduct(_context).Do(Product);
            return RedirectToPage("Index");
        }
    }
}
