﻿using Shop.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Conventions.Internal;
using Shop.Domain.Models;

namespace Shop.Application.CreateProducts
{
    public class CreateProduct
    {

        private ApplicationDbContext _context;
         public CreateProduct(ApplicationDbContext context)
         {
             _context = context;

         }

        public async Task Do(ProductViewModel vm)
        {
            _context.Products.Add(new Product
            {
               
                Name = vm.Name,
                Description = vm.Description,
                Price = vm.Price,
            });

           await _context.SaveChangesAsync();
        }
    }

    public class ProductViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
