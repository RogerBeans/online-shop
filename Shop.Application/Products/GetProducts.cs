﻿using Shop.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Shop.Application.GetProducts
{
    public class GetProducts
    {
        private ApplicationDbContext _ctx;
        public GetProducts(ApplicationDbContext context)
        {
            _ctx = context;

        }
        public IEnumerable<ProductViewModel> DO() =>
             _ctx.Products.ToList().Select(x => new ProductViewModel
            {
                Name = x.Name,
                Description = x.Description,
                Price = $"${x.Price.ToString("N2")}", // 1100.50 => 1,100.50 
            }) ;
        
    }

    public class ProductViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
    }
}
